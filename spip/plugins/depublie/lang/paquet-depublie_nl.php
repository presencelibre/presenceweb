<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-depublie?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'depublie_description' => 'Stop de publicatie van artikelen op een door jou gekozen tijdstip.',
	'depublie_nom' => 'Depublicatie',
	'depublie_slogan' => 'Depublicaties programmeren'
);
