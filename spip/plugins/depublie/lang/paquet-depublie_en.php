<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-depublie?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'depublie_description' => 'Allows you to unpublish articles at the date of your choice.',
	'depublie_nom' => 'Unpublish',
	'depublie_slogan' => 'Schedule the unpublications'
);
