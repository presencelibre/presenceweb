<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/depublie/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'depublie_description' => 'Permet de dépublier les articles à la date de votre choix.',
	'depublie_nom' => 'Dépublie',
	'depublie_slogan' => 'Programmer les dépublications'
);
