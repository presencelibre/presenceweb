<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/depublie?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Parameters for de-publishing',
	'configurer_duree_publication' => 'Duration of publishing (During validation of the article, this option automatically indicates the date of de-publication). No value inplies a manual input for the date of de-publication',
	'configurer_rubrique_depublie' => 'Only activate de-publishing for the following sectors (separated by a comma).',
	'configurer_statut_depublie' => 'Choose the status to be given at de-publication',

	// D
	'date_depublie' => 'Changes',

	// E
	'erreur_date_superieure' => 'The date of depublication cannot be prior to the publication date',

	// I
	'icone_configurer_depublie' => 'Configure de-publications',
	'icone_voir_depublie' => 'List of de-publications',
	'info_1_objet' => 'An object',
	'info_nb_objets' => '@nb@ objects',
	'info_objet' => 'Object',

	// L
	'label_jour' => 'Days',
	'label_mois' => 'Months',
	'label_publication_duree' => 'Duration',
	'label_publication_periode' => 'Period',
	'label_rubrique_depublie' => 'Identifier(s) of sub-section(s)',
	'label_secteur_depublie' => 'Identifier(s) of main-section(s)',
	'label_statut_depublie' => 'Status',

	// S
	'statut_futur' => 'Status',

	// T
	'texte_date_depublication' => 'Date of online de-publication:',
	'texte_date_depublication_nonaffichee' => 'Do not use the date of de-publication',
	'titre_page_configurer_depublies' => 'Configure de-publications',
	'titre_page_depublies_objets' => 'Objects with a programmed change of status'
);
