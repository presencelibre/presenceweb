<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/depublie?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Parametri per la de-pubblicazione',
	'configurer_duree_publication' => 'Durata della pubblicazione (durante la convalida dell’articolo, questa opzione indica automaticamente la data di de-pubblicazione). Nessun valore implica un input manuale per la de-pubblicazione',
	'configurer_rubrique_depublie' => 'Attiva la de-pubblicazione solo per i seguenti settori (separati da una virgola):',
	'configurer_statut_depublie' => 'Scegli lo stato da attribuire all’articolo una volta de-pubblicato',

	// D
	'date_depublie' => 'Cambia il',

	// E
	'erreur_date_superieure' => 'La data di de-pubblicazione non può essere antecedente alla data di pubblicazione',

	// I
	'icone_configurer_depublie' => 'Configura le de-pubblicazioni',
	'icone_voir_depublie' => 'Lista delle de-pubblicazioni',
	'info_1_objet' => 'Un oggetto',
	'info_nb_objets' => '@nb@ oggetti',
	'info_objet' => 'Oggetto',

	// L
	'label_jour' => 'Giorni',
	'label_mois' => 'Mesi',
	'label_publication_duree' => 'Durata',
	'label_publication_periode' => 'Periodo',
	'label_rubrique_depublie' => 'Identificativo(i) della(e) rubrica(che)',
	'label_secteur_depublie' => 'Identificativo(i) del(i) settore(i)',
	'label_statut_depublie' => 'Stato',

	// S
	'statut_futur' => 'Stato',

	// T
	'texte_date_depublication' => 'Data della de-pubblicazione on-line:',
	'texte_date_depublication_nonaffichee' => 'Non utilizzare la data di de-pubblicazione',
	'titre_page_configurer_depublies' => 'Configura le de-pubblicazioni',
	'titre_page_depublies_objets' => 'Oggetti con un cambio di stato programmato'
);
