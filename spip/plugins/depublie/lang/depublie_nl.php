<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/depublie?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Parameters voor depublicatie',
	'configurer_duree_publication' => 'Geef een publicatieduur aan als de status in gepubliceerd verandert, laat anders leeg.',
	'configurer_rubrique_depublie' => 'Het weergeven van het depublicatie-formulier vereist de identificatie van een of meerdere hoofd- en subrubrieken, gescheiden door een komma.',
	'configurer_statut_depublie' => 'Kies de bij depublicatie toe te wijzen status',

	// D
	'date_depublie' => 'Wijzigt op',

	// E
	'erreur_date_superieure' => 'De datum van depublicatie moet verder in de toekomst liggen dan de publicatiedatum',

	// I
	'icone_configurer_depublie' => 'Depublicaties configureren',
	'icone_voir_depublie' => 'Lijst van depublicaties',
	'info_1_objet' => 'Een object',
	'info_nb_objets' => '@nb@ objecten',
	'info_objet' => 'Object',

	// L
	'label_jour' => 'Dagen',
	'label_mois' => 'Maanden',
	'label_publication_duree' => 'Duur',
	'label_publication_periode' => 'Periode',
	'label_rubrique_depublie' => 'Identificatie van subrubriek(en)',
	'label_secteur_depublie' => 'Identificatie van hoofdrubriek(en)',
	'label_statut_depublie' => 'Status',

	// S
	'statut_futur' => 'Status',

	// T
	'texte_date_depublication' => 'Datum van depublicatie:',
	'texte_date_depublication_nonaffichee' => 'De datum van depublicatie niet gebruiken',
	'titre_page_configurer_depublies' => 'Depublicaties configureren',
	'titre_page_depublies_objets' => 'De objecten die van status gaan veranderen'
);
