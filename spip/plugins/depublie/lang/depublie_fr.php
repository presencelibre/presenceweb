<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/depublie/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Paramétrages des dépublications',
	'configurer_duree_publication' => 'Durée de la publication (Lors de la validation de l’article, cette option indique automatiquement la date de dépublication). Ne rien indiquer pour saisir manuellement la date de dépublication',
	'configurer_rubrique_depublie' => 'N’activer la dépublication que sur les rubriques et secteurs suivants (séparer les identifiants par des virgules).',
	'configurer_statut_depublie' => 'Choisissez le statut à donner lors de la dépublication',

	// D
	'date_depublie' => 'Change le',

	// E
	'erreur_date_superieure' => 'La date de dépublication doit être supérieure à la date de publication',

	// I
	'icone_configurer_depublie' => 'Configurer les dépublications',
	'icone_voir_depublie' => 'Liste des dépublications',
	'info_1_objet' => 'Un objet',
	'info_nb_objets' => '@nb@ objets',
	'info_objet' => 'Objet',

	// L
	'label_jour' => 'Jours',
	'label_mois' => 'Mois',
	'label_publication_duree' => 'Durée',
	'label_publication_periode' => 'Période',
	'label_rubrique_depublie' => 'Identifiants de rubrique(s)',
	'label_secteur_depublie' => 'Identifiants de secteur(s)',
	'label_statut_depublie' => 'Statut',

	// S
	'statut_futur' => 'Statut',

	// T
	'texte_date_depublication' => 'Date de dépublication en ligne :',
	'texte_date_depublication_nonaffichee' => 'Ne pas utiliser la date de dépublication',
	'titre_page_configurer_depublies' => 'Configurer les dépublications',
	'titre_page_depublies_objets' => 'Les objets avec un changement de statut programmé'
);
